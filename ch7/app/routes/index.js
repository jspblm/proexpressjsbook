var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/search', function(req, res, next){
  console.log(req.query);
  res.end(JSON.stringify(req.query) + '\r\n');
});

router.get('/params/:role/:name/:status', function(req, res) {
  console.log(req.params);
  console.log(req.route);
  res.end();
});

router.post('/body', function(req, res){
	console.log(req.body);
	res.end(JSON.stringify(req.body) + '\r\n');
});

router.get('/cookies', function(req, res){
	if (!req.cookies.counter)
		res.cookie('counter', 0);
	else 
		res.cookie('counter', parseInt(req.cookies.counter, 10) + 1);
	res.status(200).send('cookies are: ', req.cookies);
});

router.get('/signed-cookies', function(req, res){
	console.log("content-type", req.get('Content-Type'));
	if (!req.signedCookies.counter)
		res.cookie('counter', 0, {signed: true})
	else
		res.cookie('counter', parseInt(req.signedCookies.counter, 10) + 1, {signed: true});
	res.status(200).send('cookies are: ', req.signedCookies);
})

module.exports = router;
